import { createPool } from '@vercel/postgres';
import type { PageServerLoad } from '../$types';
import { POSTGRES_URL } from '$env/static/private';
import type { Participant, ParticipantPG } from '$lib/definitions/participant';

export const load = (async () => {
	const db = createPool({ connectionString: POSTGRES_URL });
	const participants: Participant[] = [];

	try {
		const { rows: participantsPG }: { rows: ParticipantPG[] } = await db.query(
			'SELECT "Name", "LastName", "Email", "GitlabUsername", "KaggleId" FROM "Participant"'
		);
		participantsPG.forEach((x) =>
			participants.push({
				name: x.Name,
				lastName: x.LastName,
				gitlab: x.GitlabUsername,
				kaggle: x.KaggleId,
				regDate: '21/10/2023'
			} as Participant)
		);
	} catch (error) {
		console.log(error);
	}

	return {
		participants
	};
}) satisfies PageServerLoad;
